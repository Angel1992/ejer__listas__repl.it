# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Instruccions__Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:

# Read an integer:
# a = int(input())
# Print a value:
# print(a)

k = int(input())
a = [[0] * k for i in range(k)]
for i in range(k):
  for j in range(k):
    if i + j + 1 < k:
      a[i][j] = 0
    elif i + j + 1 == k:
      a[i][j] = 1
    else:
      a[i][j] = 2
for linea in a:
  print(*linea)