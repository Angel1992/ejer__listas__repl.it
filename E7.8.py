# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Given a list of integers, find the first maximum element in it. Print its value and its index (counting with 0).

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
cont= 0
for i in range(1, len(a)):
    if a[i] > a[cont]:
        cont = i
print(a[cont], cont)