# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Given a list of numbers with all elements sorted in ascending order, determine and print the number
# of distinct elements in it.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

num = input()
lista_t = list(map(int, num.split()))
cont = 1

for n in range(1, len(lista_t)):
    if lista_t[n - 1] != lista_t[n]:
        cont += 1

print(cont)