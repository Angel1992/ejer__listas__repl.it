# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# instruccions__ It is possible to place 8 queens on an 8×8 chessboard so that no two queens threaten each other.
# Thus, it requires that no two queens share the same row, column, or diagonal.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

L1 = []
L2 = []
for i in range(8):
	A = [int(s) for s in input().split()]
	L1.append(A[0])
	L2.append(A[1])
respuesta = "NO "
for o in range(8):
	for j in range(o + 1, 8 ):
		if ((L1[o] == L1[j]) or
        (L2[o] == L2[j]) or
        (abs(L1[o] - L1[j]) == abs(L2[o] - L2[j]))):
			respuesta="YES"
print(respuesta)