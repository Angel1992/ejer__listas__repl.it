# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Given a list of numbers, print all its even elements. Use a for-loop
# that iterates over the list itself and not over its indices. That is, don't use range()

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a= input().split()
for n in range(len(a)):
  a[n]=int(a[n])
  if a[n]%2==0:
     print (a[n])
