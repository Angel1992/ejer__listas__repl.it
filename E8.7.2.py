# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Instruccions__ Given two positive integers n and m, create a two-dimensional
# array of size n×m and populate it with the characters
# "."and "*" in a chequered pattern. The top left corner should have the character "." .

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

x, y = [int(s) for s in input().split()]
a = [["."]*y for i in range(x)]
for j in range (x):
    for k in range (y):
        if k%2 != 0 and j%2 == 0:
          a[j][k] = '*'
        elif k%2 == 0 and j%2 != 0:
          a[j][k] = '*'
for line in a:
    print(*line)