# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
#Instruccions_ Given a list of distinct numbers, swap the minimum and the maximum and print the resulting list.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

my_lista = list([int(s) for s in input().split()])

max, min = my_lista.index(max(my_lista)), my_lista.index(min(my_lista))
my_lista[max], my_lista[min] = my_lista[min], my_lista[max]

print(my_lista)