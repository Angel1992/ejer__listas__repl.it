# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Given a list of numbers, determine and print the number of elements that are greater than both of their neighbors.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

t = input()
lista_q = list(map(int, t.split()))
count = 0

for j in range(1,len(lista_q)-1):
  if lista_q[j-1] < lista_q[j] and lista_q [j] > lista_q[j+1]:
    count = count + 1
print(count)