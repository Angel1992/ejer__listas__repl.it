# Given a list of numbers, find and print all its elements with even indices (i.e. A[0], A[2], A[4], ...)
# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
#print(a)

cont=0
for k in a:
  if cont %2==0:
    print (k)
  cont+=1
