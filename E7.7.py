# Autor_ Angel Bolivar Contento Guaman
# Email_ angel.b.contento@unl.edu.ec
# Given a list of numbers, swap adjacent elements in each pair (swap A[0] with A[1], A[2] with A[3], etc.).
# Print the resulting list. If a list has an odd number.
# of elements, leave the last element intact.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

numeros = input()
lista_n = list(map(int, numeros.split()))

for y in range(0, len(lista_n), 2):
    rsult = lista_n.pop(y)
    lista_n.insert(y + 1, rsult)

print(lista_n)