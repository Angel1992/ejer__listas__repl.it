# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Given a list of numbers, find and print all its elements that are greater than their left neighbor.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = [int(s) for s in input().split()]
for o in range(1, len(a)):
  if a[o - 1] < a[o]:
    print(a[o], end=' ')