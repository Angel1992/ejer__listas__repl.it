# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it
# Read an integer:
# Print a value:
# print(a)
#for

t = int(input())
primero = [i for i in range(t)]
out = [[]]*t
for j in range(t):
  out[j] = primero[j:0:-1]
  for l in primero[:t-j]:
    out[j].append(l)
  print(*out[j])
