# Autor_ Angel Bolivar Contento Guaman.
# Email_ angel.b.contento@unl.edu.ec
# Istruccions__Given two integers - the number of rows m and columns n of m×n 2d list -
# and subsequent m rows of n integers, followed by two non-negative
# integers i and j less than n, swap the columns i and j of 2d list and print the result.

# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)
p, q = [int(s) for s in input().split()]
result = [[int(k) for k in input().split()] for i in range(p)]
a, b =[int(s) for s in input().split()]
for t in range(p):
    result[t][b], result[t][a] = result[t][a], result[t][b]
    print(*result[t], sep =' ')
